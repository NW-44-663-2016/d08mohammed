﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MohammedD08.Models
{
    public class Museum
    {
       
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int MuseumID { get; set; }

        [Required]
        [Display(Name ="Name of the museum")]
        public string MuseumName { get; set; }

        [Display(Name ="Area (sqft)")]
        public double AreaInSqft { get; set; }

        [Display(Name ="Art exhibited")]
        public string ArtCollectionType { get; set; }

        [Display(Name ="Name of the art")]
        public string Art { get; set; }

       
        public int? LocationID { get; set; }

        
        public virtual Location Location { get; set; }

        public List<Location> museumSets { get; set; }

        public static List<Museum> ReadAllFromCSV(string filepath)
        {
            List<Museum> lst = File.ReadAllLines(filepath)
                                        .Skip(1)
                                        .Select(v => Museum.OneFromCsv(v))
                                        .ToList();
            return lst;
        }

        public static Museum OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Museum item = new Museum();

            int i = 0;
            item.MuseumID = Convert.ToInt32(values[i++]);
            item.MuseumName = Convert.ToString(values[i++]);
            item.AreaInSqft = Convert.ToInt32(values[i++]);
            item.Art = Convert.ToString(values[i++]);
            item.ArtCollectionType = Convert.ToString(values[i++]);

            item.LocationID = Convert.ToInt32(values[i++]);

            return item;
        }

    }
}
