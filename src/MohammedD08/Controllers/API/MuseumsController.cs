using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using MohammedD08.Models;

namespace MohammedD08.Controllers
{
    [Produces("application/json")]
    [Route("api/Museums")]
    public class MuseumsController : Controller
    {
        private ApplicationDbContext _context;

        public MuseumsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Museums
        [HttpGet]
        public IEnumerable<Museum> GetMuseums()
        {
            return _context.Museums;
        }

        // GET: api/Museums/5
        [HttpGet("{id}", Name = "GetMuseum")]
        public async Task<IActionResult> GetMuseum([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Museum museum = await _context.Museums.SingleAsync(m => m.MuseumID == id);

            if (museum == null)
            {
                return HttpNotFound();
            }

            return Ok(museum);
        }

        // PUT: api/Museums/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMuseum([FromRoute] int id, [FromBody] Museum museum)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != museum.MuseumID)
            {
                return HttpBadRequest();
            }

            _context.Entry(museum).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MuseumExists(id))
                {
                    return HttpNotFound();
                }
                else
                {
                    throw;
                }
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/Museums
        [HttpPost]
        public async Task<IActionResult> PostMuseum([FromBody] Museum museum)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            _context.Museums.Add(museum);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (MuseumExists(museum.MuseumID))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("GetMuseum", new { id = museum.MuseumID }, museum);
        }

        // DELETE: api/Museums/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMuseum([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Museum museum = await _context.Museums.SingleAsync(m => m.MuseumID == id);
            if (museum == null)
            {
                return HttpNotFound();
            }

            _context.Museums.Remove(museum);
            await _context.SaveChangesAsync();

            return Ok(museum);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MuseumExists(int id)
        {
            return _context.Museums.Count(e => e.MuseumID == id) > 0;
        }
    }
}